//
//  AudioPlayer.h
//  ERB
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "NSURL+EncryptedFileURLProtocol.h"

typedef enum{
    PLAYING = 0,
    PAUSED,
    STOPPED
}AUDIO_PLAYER_STATE;

@interface AudioPlayer : NSObject

@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (nonatomic, retain) NSString *audioFile;
@property (nonatomic, assign) CGFloat audioVolume;
@property (nonatomic, assign) AUDIO_PLAYER_STATE playerState;

- (void)configureAudioPlayerWithAudioFile:(NSString *)_audioFile withDelegate:(id)delegate;
- (void)configureAudioPlayerWithAudioFilePath:(NSString *)_audioFilePath withDelegate:(id)delegate;
- (void)configureAudioPlayerWithEncryptedAudioFile:(NSString *)_audioFile withDelegate:(id)delegate;
- (void) playAudio;
- (void) pauseAudio;
- (void) stopAudio;
- (void) resetAudioPlayer;
+ (id)sharedAudioPlayer;
@end
