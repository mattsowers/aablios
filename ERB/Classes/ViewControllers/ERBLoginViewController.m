//
//  ERBLoginViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBLoginViewController.h"
#import "ERBCheckInViewController.h"
#import "SSZipArchive.h"
#import "ERBBeginTutorialViewController.h"
#import "ERBAppleReviewerExamViewController.h"

@interface ERBLoginViewController ()
- (void) registerForKeyboardNotifications;
- (void) loginUserWithData:(NSArray *)dataArray;
- (void) customizeUI;
- (void) loginUser;
- (void) uncheckStudent;
- (BOOL) isAppleReviewer;
- (void) loadAppleReviewerExam;
- (void) shouldEnableViewElements:(BOOL)shouldEnable;
- (void) appleReviewerExamEnded;

@property (nonatomic, retain) ERBWebOperation *webServiceHelper;
@property (nonatomic, retain) ERBWebOperation *uploadResponsesWebServiceHelper;
@property (nonatomic, retain) ERBAppleReviewerExamViewController *appleReviewerExamVC;
@end


@implementation ERBLoginViewController
@synthesize delegate, activityIndicator, userName, password;
@synthesize loginButton, userLoginLabel, helpLabel;
@synthesize webServiceHelper;
@synthesize loginResponse;
@synthesize uploadResponsesWebServiceHelper;

#pragma mark - Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

#pragma mark - Private Methods

///////////////////////////////////////////////////////////////////////
//// Purpose            : Customize the UI
///////////////////////////////////////////////////////////////////////
- (void) customizeUI
{
    self.userLoginLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:46.0];
    self.userName.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:16.0];
    self.password.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:16.0];
    self.helpLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:13.5];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
        self.iOS7StatusBar.hidden = YES;
    }
    
   [self shouldEnableViewElements:YES];
    
}
///////////////////////////////////////////////////////////////////////
//// Purpose            :Registers for keyboard notifications
///////////////////////////////////////////////////////////////////////
- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void) loginUserWithData:(NSArray *)dataArray
{
    if (delegate && [delegate respondsToSelector:@selector(adminDidLogin: withData:)])
    {
        CANCEL_CONNECTION(self.webServiceHelper);
        
        [delegate adminDidLogin:YES withData:dataArray];
    }
}

///////////////////////////////////////////////////////////////////////
//// Purpose            :UIKeyboardWillShowNotification selector
///////////////////////////////////////////////////////////////////////
- (void) keyboardWillShow: (NSNotification *) notification
{
    double animationDuration = [[notification.userInfo objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y -= 70;
        self.view.frame = frame;
        CGRect statusBarframe = self.iOS7StatusBar.frame;
        statusBarframe.origin.y += 70;
        self.iOS7StatusBar.frame = statusBarframe;
    }];
}

///////////////////////////////////////////////////////////////////////
//// Purpose            :UIKeyboardWillHideNotification selector
///////////////////////////////////////////////////////////////////////
- (void) keyboardWillHide: (NSNotification *) notification
{
    double animationDuration = [[notification.userInfo objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y += 70;
        self.view.frame = frame;
        CGRect statusBarframe = self.iOS7StatusBar.frame;
        statusBarframe.origin.y -= 70;
        self.iOS7StatusBar.frame = statusBarframe;
    }];
}

- (void) uncheckStudent{
    ERBBeginTutorialViewController *beginTutorialViewController = (ERBBeginTutorialViewController *)self.delegate;
    if (beginTutorialViewController) {
        [beginTutorialViewController uncheckStudent];
    }
}

- (BOOL) isAppleReviewer{
    
    if (([self.userName.text isEqualToString:@"applereview1@erblearn.org"] && [self.password.text isEqualToString:@"ERB@747p"])
        || ([self.userName.text isEqualToString:@"applereview2@erblearn.org"] && [self.password.text isEqualToString:@"ERB@717p"])
        || ([self.userName.text isEqualToString:@"applereview3@erblearn.org"] && [self.password.text isEqualToString:@"ERB@480p"])
        ){
        return YES;
    }
    return NO;
}

- (void) loadAppleReviewerExam{
    
    [self shouldEnableViewElements:NO];
    
    // Hide the status bar for Apple reviewer exam
    [(ERBBeginTutorialViewController *)self.delegate setShouldHideStatusBar:YES];
    [(ERBBeginTutorialViewController *)self.delegate updateStatusBar];
    
    SAFE_RELEASE(_appleReviewerExamVC);
    _appleReviewerExamVC = [[ERBAppleReviewerExamViewController alloc] initWithNibName:@"ERBAppleReviewerExamViewController" bundle:nil];
    [self.view addSubview:self.appleReviewerExamVC.view];
}

- (void) shouldEnableViewElements:(BOOL)shouldEnable{
    for (UIView *subView in  [self.view subviews]) {
        if ([subView isKindOfClass:[UIButton class]]) {
            [(UIButton *)subView setEnabled:shouldEnable];
        }
        if ([subView isKindOfClass:[UITextField class]]) {
            [(UITextField *)subView setEnabled:shouldEnable];
        }
    }
}

- (void) appleReviewerExamEnded{
    [self shouldEnableViewElements:YES];
    // Hide the status bar for Apple reviewer exam
    [(ERBBeginTutorialViewController *)self.delegate setShouldHideStatusBar:NO];
    [(ERBBeginTutorialViewController *)self.delegate updateStatusBar];
    self.userName.text = @"";
    self.password.text = @"";
}

- (void) loginUser
{
    //Save the Auth Token in User Defaults
    NSString *authToken = [self.loginResponse  objectForKey:KEY_AUTH_TOKEN];
    SAVE_USER_DEFAULTS_AND_SYNC(authToken, KEY_AUTH_TOKEN);
    
    //Save the role of logged in user in User Defaults
    NSString *role = [self.loginResponse  objectForKey:RESPONSE_KEY_ROLE];
    SAVE_USER_DEFAULTS_AND_SYNC(role, RESPONSE_KEY_ROLE);
    
    //Save the name of logged in user in User Defaults
    NSString *_userName = [NSString stringWithFormat:@"%@ %@", [self.loginResponse  objectForKey:RESPONSE_KEY_FIRST_NAME], [self.loginResponse  objectForKey:RESPONSE_KEY_LAST_NAME]];
    SAVE_USER_DEFAULTS_AND_SYNC(_userName, KEY_USER_NAME);
    
    //Save the enable_client_logging in user defaults
    NSString *isLoggingEnabled = [self.loginResponse objectForKey:RESPONSE_KEY_ENABLE_CLIENT_LOGGING];
    
    [[NSUserDefaults standardUserDefaults] setBool:[isLoggingEnabled boolValue] forKey:RESPONSE_KEY_ENABLE_CLIENT_LOGGING];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //Clear the log data if logging is not enabled.
    if (![isLoggingEnabled boolValue])
    {
        [[EventLogger sharedEventLogger] clearLogs];
    }
    
    [self loginUserWithData:[loginResponse objectForKey:RESPONSE_KEY_DATA]];
}


- (void) callWebServiceToUploadResponses
{
    NSString *questionsFilePath = [[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT] stringByAppendingPathComponent:FILE_NAME_QUESTION_RESPONSES];
    NSMutableDictionary *responsesDict = [NSMutableDictionary dictionaryWithContentsOfFile:questionsFilePath];
    NSMutableArray *responses = [responsesDict objectForKey:@"responses"];
    
    if (responses.count == 0)
    {
        [responsesDict removeObjectForKey:@"responses"];
    }
    else
    {
        NSError *error = nil;
        id logJSON = [NSJSONSerialization dataWithJSONObject:responses options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[[NSString alloc] initWithData:logJSON encoding:NSUTF8StringEncoding] autorelease];
        [responsesDict setObject:jsonString forKey:@"responses"];
    }
    if (responsesDict)
    {
        if (self.uploadResponsesHoverView.hidden)
        {
            self.uploadResponsesHoverView.hidden = NO;
        }
        uploadResponsesWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_ADD_OFFLINE_RESPONSES]] withParams:responsesDict withRequestIdentifier:REQUEST_ID_UPLOAD_RESPONSE_ACTION withHttpMethod:ERB_REQUEST_POST];
        self.uploadResponsesWebServiceHelper.delegate = self;
        [self.uploadResponsesWebServiceHelper sendWebServiceRequest];
    }
}

#pragma mark - UIAlertViewDelegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1001)
    {
        switch (buttonIndex)
        {
            case 0:
                //In case of no, upload incomplete responses to server
                self.uploadResponsesHoverView.hidden = NO;
                [self callWebServiceToUploadResponses];
                break;
            case 1:
                if (delegate && [delegate respondsToSelector:@selector(testDidResumeInState:)])
                {
                    [delegate testDidResumeInState:EXAM_MODE_OFFLINE];
                }
                break;
            default:
                break;
        }
    }
    else if (alertView.tag == 1002)
    {
        switch (buttonIndex)
        {
            case 0:
                //In case of no, remove the exam state object
                [self uncheckStudent];
                [USER_DEFAULTS removeObjectForKey:KEY_EXAM_STATE];[USER_DEFAULTS synchronize];
                [self loginUser];
                break;
            case 1:
                if (delegate && [delegate respondsToSelector:@selector(testDidResumeInState:)])
                {
                    [delegate testDidResumeInState:EXAM_MODE_ONLINE];
                }
                break;
            default:
                break;
        }
    }
}

#pragma mark - Action Methods

- (IBAction)loginButtonPressed:(id)sender
{
    //Validation for empty fields
    BOOL areFieldsEmpty = [ERBCommons isStringEmpty:userName.text] || [ERBCommons isStringEmpty:password.text];
    if (areFieldsEmpty)
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_EMPTY_LOGIN_FIELDS) delegate:nil];
        return;
    }
    
    //Resign keyboard
    if([self.userName isFirstResponder])
        [self.userName resignFirstResponder];
    else if ([self.password isFirstResponder])
        [self.password resignFirstResponder];
    
    if ([self isAppleReviewer]) {
        [self loadAppleReviewerExam];
    }
    else{
        //Check internet connectivity
        if ([[ERBCommons appController] internetCheck])
        {
            [self.loginButton setEnabled:NO];
            self.activityIndicator.hidden = NO;
            [self.activityIndicator startAnimating];
            NSArray *keys = [NSArray arrayWithObjects:@"username", @"password", nil];
            NSArray *values = [NSArray arrayWithObjects:userName.text, password.text, nil];
            NSDictionary *paramsDict = nil;
            
            @try {
                paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
            if (self.webServiceHelper)
            {
                CANCEL_CONNECTION(self.webServiceHelper);
                [webServiceHelper release];
                webServiceHelper = nil;
            }
            
            webServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_AUTHENTICATION_PATH]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_AUTHENTICATE withHttpMethod:ERB_REQUEST_POST];
            [paramsDict release];
            self.webServiceHelper.delegate = self;
            [self.webServiceHelper sendWebServiceRequest];
        }
        else
        {
            [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
        }
    }
}

- (IBAction)helpButtonPressed:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(didPressHelp)])
    {
        [delegate didPressHelp];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    //Check request Id
    if ([requestId isEqualToString:REQUEST_ID_AUTHENTICATE])
    {
        [self.loginButton setEnabled:YES];
        self.activityIndicator.hidden = YES;
        NSError *error = nil;
        self.loginResponse = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        if (!error) {
            //Check the auth token, if there, login user.
            if ([[self.loginResponse  objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                if ([USER_DEFAULTS objectForKey:KEY_EXAM_STATE]) //Here show alert if exam state object exists in user defaults
                {
                    [ERBCommons showAlert:@"A test session was left in between. Do you want to resume?" delegate:self btnTitle:@"No" otherBtnTitle:@"Yes" tag:1002];
                }
                else
                {
                    [self loginUser];
                }
            }
            else
            {
                [ERBCommons showAlert:[loginResponse  objectForKey:RESPONSE_KEY_ERROR_MESSAGE] delegate:nil];
            }
        }
        else{
            DLog(@"Error while parsing JSON response for WS: %@", requestId);
        }
        
    }
    else if ([requestId isEqualToString:REQUEST_ID_UPLOAD_RESPONSE_ACTION])
    {
        NSError *error = nil;
        NSDictionary *responeDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        if (!error) {
            //Check the auth token, if there, login user.
            if ([[responeDict  objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                self.uploadResponsesHoverView.hidden = YES;
                NSMutableDictionary *responsesDict = [NSMutableDictionary dictionaryWithContentsOfFile:[[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT] stringByAppendingPathComponent:FILE_NAME_QUESTION_RESPONSES]];
                [ERBCommons showAlert:[NSString stringWithFormat:@"Data upload for Student %@ was successful", [responsesDict objectForKey:RESPONSE_KEY_STUDENT_NAME]] delegate:nil];
                
                [ERBCommons removeOfflineContent];
                SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithInteger:ERB_EXAM_STATE_NONE], KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE);
            }
            
        }
        else{
            DLog(@"Error while parsing JSON response for WS: %@", requestId);
        }
    }
    
    [self.activityIndicator stopAnimating];
}

- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if ([requestId isEqualToString:REQUEST_ID_AUTHENTICATE])
    {
        [self.loginButton setEnabled:YES];
        [self.activityIndicator stopAnimating];
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
    else if([requestId isEqualToString:REQUEST_ID_UPLOAD_RESPONSE_ACTION])
    {
        [self performSelector:@selector(callWebServiceToUploadResponses) withObject:nil afterDelay:DELAY];
    }
    
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:userName]) {
        [password becomeFirstResponder];
    }
    else if ([textField isEqual:password])
    {
        //Login user
        [textField resignFirstResponder];
        [self loginButtonPressed: nil];
    }
    return NO;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Custmoize the UI
    [self customizeUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appleReviewerExamEnded) name:@"AppleReviewerTestEnded" object:nil];
    //Register for keyboard notification
    [self registerForKeyboardNotifications];
    //Check the exam state and proceed accordingly
    ERB_OFFLINE_EXAM_STATE examState = [[USER_DEFAULTS objectForKey:KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE] intValue];
    if (examState == ERB_EXAM_STATE_BEGIN_TUTORIAL_SCREEN || examState == ERB_EXAM_STATE_TUTORIAL || examState == ERB_EXAM_STATE_IN_PROGRESS)
    {
        SAVE_USER_DEFAULTS_AND_SYNC(EXAM_MODE_OFFLINE, RESPONSE_KEY_ADMIN_EXAM_MODE);
        [ERBCommons showAlert:@"A test session was left in between. Do you want to resume?" delegate:self btnTitle:@"No" otherBtnTitle:@"Yes" tag:1001];
    }
    else if(examState == ERB_EXAM_STATE_UPLOAD_RESPONSES)
    {
        // Call the web service to send the saved responses to server
        [self callWebServiceToUploadResponses];
    }
    else
    {
        [ERBCommons removeOfflineContent];
        SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithInteger:ERB_EXAM_STATE_NONE], KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void) dealloc
{
    //Deregister for keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SAFE_RELEASE(loginResponse);
    SAFE_RELEASE(webServiceHelper);
    SAFE_RELEASE(uploadResponsesWebServiceHelper);
    [activityIndicator release];
    [userName release];
    [password release];
    [loginButton release];
    [userLoginLabel release];
    [helpLabel release];
    [_iOS7StatusBar release];
    [_uploadResponsesHoverView release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setActivityIndicator:nil];
    [self setUserName:nil];
    [self setPassword:nil];
    [self setLoginButton:nil];
    [self setUserLoginLabel:nil];
    [self setHelpLabel:nil];
    [self setIOS7StatusBar:nil];
    [self setUploadResponsesHoverView:nil];
    [super viewDidUnload];
}
@end
