//
//  ERBHelpViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import <UIKit/UIKit.h>

@interface ERBHelpViewController : UIViewController
- (IBAction)doneButtonPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;
@property (retain, nonatomic) IBOutlet UITextView *helpTextView;

@end
