//
//  ERBScheduleViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBScheduleViewController.h"
#import "ERBScheduleCell.h"

@interface ERBScheduleViewController ()
@property (nonatomic, retain) NSString *selectedExamId;
@property (nonatomic, retain) ERBWebOperation *webServiceHelper;
- (void) customizeUI;
@end

@implementation ERBScheduleViewController

// Dictionary object to store currently launched schedule.
NSDictionary *currentlyLaunchedSchedule;

@synthesize delegate, scheduleTableView, scheduleArray;
@synthesize activityIndicator, userNameLabel, examDate;
@synthesize selectedExamId;
@synthesize schoolName,todaysScheduleLabel, timeLabel, testEventLabel, studentLabel, supervisorLabel, administratorsLabel;
@synthesize webServiceHelper;

#pragma mark - Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Private Methods

///////////////////////////////////////////////////////////////////////
//// Purpose            : Customize the UI
///////////////////////////////////////////////////////////////////////
- (void) customizeUI
{
    self.userNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:11.0];
    self.schoolName.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:24.0];
    self.examDate.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:12.0];
    self.todaysScheduleLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:20.0];
    self.timeLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.testEventLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.studentLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.supervisorLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.administratorsLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
}

#pragma mark - ERBCheckInCellDelegate
-(void)didPressLaunchNowWithTag:(int) tag
{
    //Call the web service to get the checkin List
    if ([[ERBCommons appController] internetCheck])
    {
        //Disable the logout button
        self.logoutButton.enabled = NO;
        
        [self.scheduleTableView setUserInteractionEnabled:NO];
        currentlyLaunchedSchedule = [self.scheduleArray objectAtIndex:tag];
        
        //Get the exam Id to pass in to the web service
        NSString *examId = [currentlyLaunchedSchedule objectForKey:REQUEST_POST_KEY_EXAM_ID];
        
        //Set the exam Id in iVar to pass in to the CheckIn ViewController. This will be used to check the user in.
        self.selectedExamId = examId;
        
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN, REQUEST_POST_KEY_EXAM_ID, REQUEST_POST_KEY_DIGEST, nil];
        NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken], examId, @"", nil];
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        
        if (self.webServiceHelper) {
            
            CANCEL_CONNECTION(self.webServiceHelper);
            
            [webServiceHelper release];
            webServiceHelper = nil;
        }
        
        webServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_FETCH_EXAM_LIST_PATH]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_CHECKIN_WS withHttpMethod:ERB_REQUEST_POST];
        
        [paramsDict release];
        self.webServiceHelper.delegate = self;
        [self.webServiceHelper sendWebServiceRequest];
        
        [self.activityIndicator startAnimating];
        [self.scheduleTableView setUserInteractionEnabled:NO];
    }
    else
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    //Check request Id
    if ([requestId isEqualToString:REQUEST_ID_CHECKIN_WS])
    {
        self.logoutButton.enabled = YES;
        [self.scheduleTableView setUserInteractionEnabled:YES];
        NSError *error = nil;
        NSDictionary *checkInResponse = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        if (!error) {
            //Check the auth token, if there, login user.
            if ([[checkInResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                NSArray *checkInDataArray = [checkInResponse objectForKey:RESPONSE_KEY_DATA];
                
                //Check if the array is empty, if Yes, there are no exams to display, dont notify delegate, back off.
                if (checkInDataArray)
                {
                    if (delegate && [delegate respondsToSelector:@selector(launchExamWithLaunchData:scheduleData:)])
                    {
                        CANCEL_CONNECTION(self.webServiceHelper);
                        [delegate launchExamWithLaunchData:checkInDataArray scheduleData:currentlyLaunchedSchedule];
                    }
                }
                else
                {
                    [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_NO_EXAMS_TO_LAUNCH) delegate:nil];
                }
            }
            else
            {
                [ERBCommons showAlert:[checkInResponse  objectForKey:RESPONSE_KEY_ERROR_MESSAGE] delegate:nil];
            }
        }
        else{
            DLog(@"Error while parsing JSON response for WS: %@", requestId);
        }
    }
    
    [self.scheduleTableView setUserInteractionEnabled:YES];
    [self.activityIndicator stopAnimating];
}

- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if ([requestId isEqualToString:REQUEST_ID_CHECKIN_WS])
    {
        self.logoutButton.enabled = YES;
        [self.scheduleTableView setUserInteractionEnabled:YES];
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
    [self.scheduleTableView setUserInteractionEnabled:YES];
    [self.activityIndicator stopAnimating];
}

#pragma mark - UIAlertViewDelegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            //Cancel Pressed, do nothing.
            break;
        case 1:
            //Notify delegate
            
            if (delegate && [delegate respondsToSelector:@selector(adminDidLogoutFromScheduleView:)])
            {
                [delegate adminDidLogoutFromScheduleView:YES];
            }
            
            break;
        default:
            break;
    }
}

#pragma mark - Action Methods
- (IBAction)logoutButtonPressed:(id)sender {
    
    UIAlertView *confirmationAlert = [[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure you want to Log Out?" delegate:self cancelButtonTitle:ERBLocalizedString(LOCALIZATION_KEY_TITLE_CANCEL) otherButtonTitles:ERBLocalizedString(LOCALIZATION_KEY_TITLE_CONFIRM), nil];
    [confirmationAlert show];
    [confirmationAlert release];
}


#pragma mark - UITableViewDelegate Methods
- (NSInteger) tableView: (UITableView *) tableView numberOfRowsInSection:(NSInteger) section
{
    return [self.scheduleArray count];
}

- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView
{
    return 1;
}

-(UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    NSDictionary *schedule = [self.scheduleArray objectAtIndex:indexPath.row];
    
    static NSString *scheduleCellIdentifier = CELL_IDENTIFER_ERBSCHEDULECELL;
    ERBScheduleCell *scheduleCell = [tableView dequeueReusableCellWithIdentifier:scheduleCellIdentifier];
    scheduleCell.delegate = self;
    
    //Assign tag to each launch button as the row
    scheduleCell.launchButton.tag = indexPath.row;
    
    //creating time schedule string
    NSString *startTime = [schedule objectForKey:RESPONSE_KEY_START_TIME];
    NSString *endTime = [schedule objectForKey:RESPONSE_KEY_END_TIME];
    NSString *timeSchedule = [ERBCommons returnTimeScheduleStringWithStartTime:startTime endTime:endTime];
    
    scheduleCell.timeLabel.text = [timeSchedule lowercaseString];
    scheduleCell.examLabel.text = [schedule objectForKey:RESPONSE_KEY_EXAM_NAME];
    scheduleCell.studentsLabel.text = [schedule objectForKey:RESPONSE_KEY_STUDENTS];
    scheduleCell.supervisorLabel.text = [[schedule objectForKey:RESPONSE_KEY_SUPERVISOR_NAMES] componentsJoinedByString:@", "];
    scheduleCell.administratorsLabel.text = [[schedule objectForKey:RESPONSE_KEY_ADMIN_NAMES] componentsJoinedByString:@", "];
    
    scheduleCell.timeLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    scheduleCell.examLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    scheduleCell.supervisorLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    scheduleCell.studentsLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    scheduleCell.administratorsLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    
    return scheduleCell;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customizeUI];
    
    [self.scheduleTableView registerNib:[UINib nibWithNibName:@"ERBScheduleCell" bundle:nil] forCellReuseIdentifier:CELL_IDENTIFER_ERBSCHEDULECELL];
    self.userNameLabel.text = [ERBCommons getUserName];
    
    //To get the user name, get the object at first index.
    if (self.scheduleArray.count > 0)
    {
        NSDictionary *schedule = [self.scheduleArray objectAtIndex:0];
        self.schoolName.text = [schedule objectForKey:RESPONSE_KEY_SCHOOL_NAME];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //creating exam-date string
    if (self.scheduleArray.count > 0) {
        self.examDate.text = [ERBCommons examDateAfterFormattingFromString:[[self.scheduleArray objectAtIndex:0] objectForKey:RESPONSE_KEY_START_TIME]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    SAFE_RELEASE(webServiceHelper);
    [scheduleTableView release];
    [scheduleArray release];
    [activityIndicator release];
    SAFE_RELEASE(selectedExamId);
    [examDate release];
    [userNameLabel release];
    [schoolName release];
    [todaysScheduleLabel release];
    [timeLabel release];
    [testEventLabel release];
    [studentLabel release];
    [supervisorLabel release];
    [administratorsLabel release];
    [_logoutButton release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScheduleTableView:nil];
    [self setScheduleArray:nil];
    [self setActivityIndicator:nil];
    [self setExamDate:nil];
    [self setUserNameLabel:nil];
    [self setSchoolName:nil];
    [self setTodaysScheduleLabel:nil];
    [self setTimeLabel:nil];
    [self setTestEventLabel:nil];
    [self setStudentLabel:nil];
    [self setSupervisorLabel:nil];
    [self setAdministratorsLabel:nil];
    [self setLogoutButton:nil];
    [super viewDidUnload];
}
@end
