//
//  ERBCheckInViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBCheckInViewController.h"
#import "ERBLoginViewController.h"
#import "NSData+CommonCrypto.h"

@interface ERBCheckInViewController ()
@property (nonatomic, retain) ERBLoginViewController *loginViewController;
@property (nonatomic, retain) ERBWebOperation *checkinWebServiceHelper;
@property (nonatomic, retain) ERBWebOperation *refreshCheckinWebServiceHelper;

- (void) callCheckInWebServiceForRow: (int) row;
- (void) configureUI;
- (void) startPolling;
- (void) stopPolling;
@end

@implementation ERBCheckInViewController
@synthesize delegate, checkinTableView, activityIndicator;
@synthesize checkInDataArray;
@synthesize examId;
@synthesize checkinDictionary, checkinData;
@synthesize dateLabel, timeLabel, userNameLabel, schoolNameLabel;
@synthesize examData;
@synthesize verifyLabel, nameLabel, dobLabel, studentCheckInLabel, genderLabel;
@synthesize loginViewController, userName;
@synthesize backButton, logoutButton;
@synthesize checkinWebServiceHelper, refreshCheckinWebServiceHelper;
@synthesize examMode;

#pragma mark - Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

#pragma mark - ERBCheckInCellDelegate
- (void) didPressVerfiyWithTag: (NSInteger)tag
{
    //Set the image for selected check button
    ERBCheckInCell *checkInCell = (ERBCheckInCell *)[checkinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:0]];
    [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_CHECK_BLUE] forState:UIControlStateNormal];
    
    NSDictionary *checkInDataDict = [self.checkInDataArray objectAtIndex:tag];
    NSString *alertTitle = [checkInDataDict objectForKey:RESPONSE_KEY_STUDENT_NAME];
    NSString *alertMsg = [NSString stringWithFormat:@"%@, born %@",[checkInDataDict objectForKey:RESPONSE_KEY_GENDER], [checkInDataDict objectForKey:RESPONSE_KEY_DOB]];
    
    //Show confirmation alert
    UIAlertView *confirmationAlert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMsg delegate:self cancelButtonTitle:ERBLocalizedString(LOCALIZATION_KEY_TITLE_CANCEL) otherButtonTitles:ERBLocalizedString(LOCALIZATION_KEY_TITLE_CONFIRM), nil];
    confirmationAlert.tag = -tag;
    [confirmationAlert show];
    [confirmationAlert release];
}

#pragma mark - UIAlertViewDelegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == -999) {
        if (delegate && [delegate respondsToSelector:@selector(student: didCheckIn: withData:)])
        {
            [self backButtonPressed:nil];
        }
    }
    else{
        ERBCheckInCell *checkInCell = (ERBCheckInCell *)[checkinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:alertView.tag*(-1) inSection:0]];
        switch (buttonIndex)
        {
            case 0:
                //Set the image for check button if checkin is cancelled
                [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_UNCHECK_GRAY] forState:UIControlStateNormal];
                break;
            case 1:
                //Notify delegate
                if (alertView.tag <= 0)
                {
                    //Call checkin Web Service
                    [self callCheckInWebServiceForRow:-(alertView.tag)];
                }
                else if (alertView.tag == 1) {
                    
                    if (delegate && [delegate respondsToSelector:@selector(adminDidLogout:)])
                    {
                        CANCEL_CONNECTION(self.checkinWebServiceHelper);
                        CANCEL_CONNECTION(self.refreshCheckinWebServiceHelper);
                        
                        // Stop polling for refreshed checkin list when checking in a student
                        [self stopPolling];
                        
                        [delegate adminDidLogout:YES];
                    }
                }
                break;
            default:
                break;
        }
    }
}

#pragma mark - Private Methods
- (void) configureUI
{
    self.verifyLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.nameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.genderLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.dobLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:15.0];
    self.studentCheckInLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:20.0];
    self.schoolNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOLD size:24.5];
    self.userNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:12.0];
    self.dateLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:12.0];
    self.timeLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:12.0];
    
    self.schoolNameLabel.text = self.examData.schoolName;
    [self.timeLabel setText:[NSString stringWithFormat:@"%@ %@", [self.examData.examTime uppercaseString], [self.examData.examName uppercaseString] ]];
    [self.dateLabel setText:[self.examData.examDate uppercaseString]];
    [self.userNameLabel setText:[ERBCommons getUserName]];
}

- (void) startPolling{
//    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(callWebServiceToRefreshData) userInfo:nil repeats:YES];
}

- (void) stopPolling{
//    [timer invalidate];
//    timer = nil;
}

- (void) callWebServiceToRefreshData
{
    [self.activityIndicator startAnimating];

    self.view.userInteractionEnabled = NO;

    //Disable back and logout buttons
    self.backButton.enabled = NO;
    self.logoutButton.enabled = NO;

    //Call the web service to get the latest checkin List
    if ([[ERBCommons appController] internetCheck])
    {
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN, REQUEST_POST_KEY_EXAM_ID, REQUEST_POST_KEY_DIGEST, nil];
        NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken], self.examId, @"", nil];
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if (self.refreshCheckinWebServiceHelper) {
            
            CANCEL_CONNECTION(self.refreshCheckinWebServiceHelper);
            
            [refreshCheckinWebServiceHelper release];
            refreshCheckinWebServiceHelper = nil;
            
        }
        
        refreshCheckinWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_FETCH_EXAM_LIST_PATH]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_CHECKIN_WS withHttpMethod:ERB_REQUEST_POST];
        [paramsDict release];
        
        self.refreshCheckinWebServiceHelper.delegate = self;
        [self.refreshCheckinWebServiceHelper sendWebServiceRequest];
        
    }
}

- (void) callCheckInWebServiceForRow: (int) row
{
    //Call the web service to get the checkin List
    if ([[ERBCommons appController] internetCheck])
    {
        //Disable back and logout buttons
        self.backButton.enabled = NO;
        self.logoutButton.enabled = NO;
        
        NSDictionary *currentlyCheckedInStudentDict = [self.checkInDataArray objectAtIndex:row];
        
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN, REQUEST_POST_KEY_EXAM_ID, RESPONSE_KEY_STUDENT_ID, nil];
        NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken], self.examId, [currentlyCheckedInStudentDict objectForKey:RESPONSE_KEY_STUDENT_ID], nil];
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        ERBCheckIn *checkin = [[ERBCheckIn alloc]init];
        checkin.examId = self.examId;
        checkin.studentId = [currentlyCheckedInStudentDict objectForKey:RESPONSE_KEY_STUDENT_ID];
        
        checkin.studentName = [NSString stringWithFormat:@"%@ %@", [currentlyCheckedInStudentDict objectForKey:RESPONSE_KEY_FIRST_NAME], [currentlyCheckedInStudentDict objectForKey:RESPONSE_KEY_LAST_NAME]];
        
        self.checkinData = checkin;
        [checkin release];
        
        if (self.checkinWebServiceHelper)
        {
            CANCEL_CONNECTION(self.checkinWebServiceHelper);
            
            [checkinWebServiceHelper release];
            checkinWebServiceHelper = nil;
        }
        
        checkinWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_CHECK_IN_PATH]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_STUDENT_CHECKIN_WS withHttpMethod:ERB_REQUEST_POST];
        [paramsDict release];
        self.checkinWebServiceHelper.delegate = self;
        [self.checkinWebServiceHelper sendWebServiceRequest];
        
        [self.activityIndicator startAnimating];
        
        // To find te name of checked in student set tag of activity indicator as the row selected
        self.activityIndicator.tag = row;
        
        [self.checkinTableView setUserInteractionEnabled:NO];
    }
    else
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
}

#pragma mark - Action Methods
- (IBAction)logoutButtonPressed:(id)sender
{
    UIAlertView *confirmationAlert = [[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure you want to Log Out?" delegate:self cancelButtonTitle:ERBLocalizedString(LOCALIZATION_KEY_TITLE_CANCEL) otherButtonTitles:ERBLocalizedString(LOCALIZATION_KEY_TITLE_CONFIRM), nil];
    confirmationAlert.tag = 1;
    [confirmationAlert show];
    [confirmationAlert release];
}

- (IBAction)backButtonPressed:(id)sender
{
    
    if (delegate && [delegate respondsToSelector:@selector(didPressBackFromCheckIn)])
    {
        // Stop polling for refreshed checkin list when checking in a student
        [self stopPolling];
        CANCEL_CONNECTION(self.checkinWebServiceHelper);
        CANCEL_CONNECTION(self.refreshCheckinWebServiceHelper);

        [delegate didPressBackFromCheckIn];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    NSError *error = nil;
    NSDictionary *checkInResponse = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
 
    if (!error) {
        //Set the help flag
        NSString *isHelpMode = [checkInResponse objectForKey:RESPONSE_KEY_HELP_HAND];
        
        if ([isHelpMode isEqualToString:@"ON"])
        {
            SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithBool:YES], RESPONSE_KEY_HELP_HAND);
        }
        else if ([isHelpMode isEqualToString:@"OFF"])
        {
            SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithBool:NO], RESPONSE_KEY_HELP_HAND);
        }
        
        if ([[checkInResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
        {
            //Save the examMode in iVar
            self.examMode = [checkInResponse objectForKey:RESPONSE_KEY_ADMIN_EXAM_MODE];
            
            // Save the exam mode for state retention
            SAVE_USER_DEFAULTS_AND_SYNC(self.examMode, RESPONSE_KEY_ADMIN_EXAM_MODE);
            
            // Save the number of questions per section
            SAVE_USER_DEFAULTS_AND_SYNC([checkInResponse objectForKey:RESPONSE_KEY_QUESTIONS_COUNT], RESPONSE_KEY_QUESTIONS_COUNT);
            
            //Set the exam state as tutorial
            if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_OFFLINE])
            {
                SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithInteger:ERB_EXAM_STATE_BEGIN_TUTORIAL_SCREEN], KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE)
            }
            
            //Save the exam mode in app controller variable to be accessed globally
            [ERBCommons appController].examMode = [checkInResponse objectForKey:RESPONSE_KEY_ADMIN_EXAM_MODE];
            
            //Check request Id
            if ([requestId isEqualToString:REQUEST_ID_STUDENT_CHECKIN_WS])
            {
                //Enable the back and logout buttons
                self.backButton.enabled = YES;
                self.logoutButton.enabled = YES;
                
                //Seat number now comes in check in response, save it.
                self.checkinData.seatNumber = [checkInResponse objectForKey:RESPONSE_KEY_STUDENT_SEAT];
                
                NSArray *allQuestionsArray = nil;
                //Extract all questions if it is offline mode
                if ([self.examMode isEqualToString:EXAM_MODE_OFFLINE])
                {
                    //Set no to disable client logging in case of offline mode..
                    SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithBool:NO], RESPONSE_KEY_ENABLE_CLIENT_LOGGING);
                    
                    allQuestionsArray = [[NSArray alloc] initWithArray:[checkInResponse objectForKey:RESPONSE_KEY_ALL_QUESTIONS]];
                    //Create directory
                    NSError *error = nil;
                    [[NSFileManager defaultManager] createDirectoryAtPath:[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT] withIntermediateDirectories:YES attributes:nil error:&error];
                    
                    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                    [def setObject:[NSKeyedArchiver archivedDataWithRootObject:allQuestionsArray] forKey:KEY_OFFLINE_QUESTIONS];
                    [def synchronize];
                    
                    //Create the responses file if it does not already exist
                    NSString *questionsFilePath = [[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT] stringByAppendingPathComponent:FILE_NAME_QUESTION_RESPONSES];
                    if (![[NSFileManager defaultManager] fileExistsAtPath:questionsFilePath])
                    {
                        NSMutableDictionary *responses = [[NSMutableDictionary alloc] init];
                        NSMutableArray *responsesArray = [[NSMutableArray alloc] init];
                        
                        //Add values
                        [responses setObject:[ERBCommons authToken] forKey:REQUEST_POST_KEY_AUTH_TOKEN];
                        [responses setObject:self.checkinData.examId forKey:REQUEST_POST_KEY_EXAM_ID];
                        [responses setObject:self.checkinData.studentId forKey:REQUEST_POST_KEY_STUDENT_ID];
                        [responses setObject:self.checkinData.studentName forKey:RESPONSE_KEY_STUDENT_NAME];
                        [responses setObject:self.checkinData.seatNumber forKey:RESPONSE_KEY_STUDENT_SEAT];
                        [responses setObject:responsesArray forKey:REQUEST_POST_KEY_RESPONSES_ARRAY];
                        [responses writeToFile:questionsFilePath atomically:YES];
                        SAFE_RELEASE(responses); //Released ..
                        SAFE_RELEASE(responsesArray); //Released ..
                    }
                    else
                    {
                        NSMutableDictionary *responsesDict = [NSMutableDictionary dictionaryWithContentsOfFile:questionsFilePath];
                        
                        [responsesDict setObject:self.checkinData.examId forKey:REQUEST_POST_KEY_EXAM_ID];
                        [responsesDict setObject:self.checkinData.studentId forKey:REQUEST_POST_KEY_STUDENT_ID];
                        [responsesDict setObject:self.checkinData.studentName forKey:RESPONSE_KEY_STUDENT_NAME];
                        [responsesDict setObject:self.checkinData.seatNumber forKey:RESPONSE_KEY_STUDENT_SEAT];
                        [responsesDict writeToFile:questionsFilePath atomically:YES];
                    }
                }
                
                if (delegate && [delegate respondsToSelector:@selector(student: didCheckIn: withData: withQuestions:)])
                {
                    // Stop polling for refreshed checkin list when checking in a student
                    [self stopPolling];
                    
                    CANCEL_CONNECTION(self.checkinWebServiceHelper);
                    CANCEL_CONNECTION(self.refreshCheckinWebServiceHelper);
                    self.view.userInteractionEnabled = NO;
                    [delegate student:self.checkinData.studentName didCheckIn:YES withData:self.checkinData  withQuestions: allQuestionsArray?allQuestionsArray : nil];
                }
                SAFE_RELEASE(allQuestionsArray);
            }
            else if ([requestId isEqualToString:REQUEST_ID_CHECKIN_WS])
            {
                self.checkInDataArray = [checkInResponse objectForKey:RESPONSE_KEY_DATA];
                
                //Check if the array is empty, if Yes, there are no exams to display, dont notify delegate, back off.
                if (checkInDataArray)
                {
                    [self.checkinTableView reloadData];
                }
                //Enable the user interaction
                self.view.userInteractionEnabled = YES;
                //Enable the back and logout buttons
                self.backButton.enabled = YES;
                self.logoutButton.enabled = YES;
            }
        }
        else if ([[checkInResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == -1){
            if ([requestId isEqualToString:REQUEST_ID_STUDENT_CHECKIN_WS])
            {
                ERBCheckInCell *checkInCell = (ERBCheckInCell *)[checkinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.activityIndicator.tag inSection:0]];
                [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_UNCHECK_GRAY] forState:UIControlStateNormal];
                
                [ERBCommons showAlert:[checkInResponse  objectForKey:RESPONSE_KEY_ERROR_MESSAGE] delegate:self btnTitle:@"OK" otherBtnTitle:nil tag:-999];
                
            }
        }
        else if ([[checkInResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == -2)
        {
            //If exam mode is none, rollback
            [ERBCommons showAlert:@"Set the exam mode from admin portal before checking in." delegate:nil];
            //Enable the back and logout buttons
            self.backButton.enabled = YES;
            self.logoutButton.enabled = YES;
            [self.checkinTableView reloadData];
            [self.checkinTableView setUserInteractionEnabled:YES];
            [self.activityIndicator stopAnimating];
            
            return;
            
        }
        else{
            if ([requestId isEqualToString:REQUEST_ID_STUDENT_CHECKIN_WS])
            {
                ERBCheckInCell *checkInCell = (ERBCheckInCell *)[checkinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.activityIndicator.tag inSection:0]];
                [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_UNCHECK_GRAY] forState:UIControlStateNormal];
                
                [ERBCommons showAlert:[checkInResponse  objectForKey:RESPONSE_KEY_ERROR_MESSAGE] delegate:nil];
            }
        }

    }
    else{
        DLog(@"Error while parsing JSON response for WS: %@", requestId);
    }
    
    [self.checkinTableView setUserInteractionEnabled:YES];
    
    [self.activityIndicator stopAnimating];
}

- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if ([requestId isEqualToString:REQUEST_ID_STUDENT_CHECKIN_WS])
    {
        //Enable the back and logout buttons
        //Disable back and logout buttons
        self.backButton.enabled = YES;
        self.logoutButton.enabled = YES;
        
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
        
        
        ERBCheckInCell *checkInCell = (ERBCheckInCell *)[checkinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.activityIndicator.tag inSection:0]];
        [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_UNCHECK_GRAY] forState:UIControlStateNormal];
        
        [self.checkinTableView setUserInteractionEnabled:YES];
        
        [self.activityIndicator stopAnimating];
    }
}

#pragma mark - UITableViewDelegate Methods
- (NSInteger) tableView: (UITableView *) tableView numberOfRowsInSection:(NSInteger) section
{
    return self.checkInDataArray.count;
}

- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView
{
    return 1;
}

-(UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    static NSString *checkinCellIdentifier = CELL_IDENTIFER_ERBCHECKINCELL;
    ERBCheckInCell *checkInCell = [tableView dequeueReusableCellWithIdentifier:checkinCellIdentifier];

    checkInCell.delegate = self;
    
    //Assign tag to each checkmark button as the row
    checkInCell.verifyButton.tag = indexPath.row;
    
    //Get the dictionary
    NSDictionary *checkInDataDict = [self.checkInDataArray objectAtIndex:indexPath.row];
    
    [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_UNCHECK_GRAY] forState:UIControlStateNormal];
    
    checkInCell.nameLabel.text = [checkInDataDict objectForKey:RESPONSE_KEY_STUDENT_NAME];
    checkInCell.genderLabel.text = [checkInDataDict objectForKey:RESPONSE_KEY_GENDER];
    checkInCell.dobLabel.text = [checkInDataDict objectForKey:RESPONSE_KEY_DOB];
    
    //Set fonts
    checkInCell.nameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    checkInCell.genderLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    checkInCell.dobLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:15.0];
    
    //Set the verified cell image accordingly
    if ([[checkInDataDict objectForKey:RESPONSE_KEY_VERIFIED] isEqualToString:@"YES"])
    {
        //Disable the cell's verify button
        [checkInCell.verifyButton setEnabled:NO];
        [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_CHECK_GRAY] forState:UIControlStateDisabled];
        
        // Check if administrator is proctor for this particular child or not
        if ( [[checkInDataDict objectForKey:RESPONSE_KEY_IS_PROCTOR] isEqualToString:@"NO"])
        {
            //Set the background view and selection style for verified cell
            UIImageView *bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, checkInCell.frame.size.width, checkInCell.frame.size.height)];
            bgImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"gray_bgbar.png")];
            [checkInCell setBackgroundView:bgImageView];
            [bgImageView release];
        }
        else
        {
            UIImageView *bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, checkInCell.frame.size.width, checkInCell.frame.size.height)];
            bgImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"green_bgbar.png")];
            [checkInCell setBackgroundView:bgImageView];
            [bgImageView release];
        }
    }
    else{
        //Enable the cell's verify button
        [checkInCell.verifyButton setEnabled:YES];
        [checkInCell.verifyButton setImage:[UIImage imageNamed:IMAGE_NAME_UNCHECK_GRAY] forState:UIControlStateDisabled];
        checkInCell.backgroundView = nil;
    }
    return checkInCell;
}


#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
        
    //Register nib for cell reuse
    [self.checkinTableView registerNib:[UINib nibWithNibName:@"ERBCheckInCell" bundle:nil] forCellReuseIdentifier:CELL_IDENTIFER_ERBCHECKINCELL];
    [self configureUI];
    
    [self startPolling];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc
{
    [checkinTableView release];
    [checkinData release];
    [checkinDictionary release];
    [checkInDataArray release];
    SAFE_RELEASE(checkinWebServiceHelper);
    SAFE_RELEASE(refreshCheckinWebServiceHelper);
    
    [examId release];
    [activityIndicator release];
    [dateLabel release];
    [timeLabel release];
    [userNameLabel release];
    [schoolNameLabel release];
    [examData release];
    
    [verifyLabel release];
    [nameLabel release];
    [genderLabel release];
    [dobLabel release];
    [studentCheckInLabel release];
    [backButton release];
    [logoutButton release];
    SAFE_RELEASE(examMode);
    [super dealloc];
}
- (void)viewDidUnload
{
    [self setExamId:nil];
    [self setCheckInDataArray:nil];
    [self setExamData:nil];
    [self setCheckinData:nil];
    [self setCheckinDictionary:nil];
    [self setCheckinTableView:nil];
    [self setActivityIndicator:nil];
    [self setDateLabel:nil];
    [self setTimeLabel:nil];
    [self setUserNameLabel:nil];
    [self setDateLabel:nil];
    [self setTimeLabel:nil];
    [self setUserNameLabel:nil];
    [self setSchoolNameLabel:nil];
    [self setVerifyLabel:nil];
    [self setNameLabel:nil];
    [self setGenderLabel:nil];
    [self setDobLabel:nil];
    [self setStudentCheckInLabel:nil];
    [self setBackButton:nil];
    [self setLogoutButton:nil];
    [super viewDidUnload];
}



@end
