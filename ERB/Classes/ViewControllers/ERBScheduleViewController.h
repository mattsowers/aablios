//
//  ERBScheduleViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBScheduleCell.h"
#import "ERBWebOperation.h"

@protocol ERBScheduleViewController <NSObject>
- (void) launchExamWithLaunchData: (id)launchData scheduleData:(NSDictionary *) scheduleData;
- (void)adminDidLogoutFromScheduleView: (BOOL) didLogout;
@end

@interface ERBScheduleViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ERBScheduleCellDelegate,ERBWebServiceDelegate>

@property (nonatomic, assign) id <ERBScheduleViewController> delegate;
@property (retain, nonatomic) IBOutlet UITableView *scheduleTableView;
@property (nonatomic, retain) NSArray *scheduleArray;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UILabel *examDate;
@property (retain, nonatomic) IBOutlet UILabel *userNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *schoolName;
@property (retain, nonatomic) IBOutlet UILabel *todaysScheduleLabel;
@property (retain, nonatomic) IBOutlet UILabel *timeLabel;
@property (retain, nonatomic) IBOutlet UILabel *testEventLabel;
@property (retain, nonatomic) IBOutlet UILabel *studentLabel;
@property (retain, nonatomic) IBOutlet UILabel *supervisorLabel;
@property (retain, nonatomic) IBOutlet UILabel *administratorsLabel;
@property (retain, nonatomic) IBOutlet UIButton *logoutButton;

- (IBAction)logoutButtonPressed:(id)sender;
@end
