//
//  ERBReusableViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol ERBReusableViewControllerDelegate <NSObject>
- (void) examDidEnd: (BOOL) didEnd ;
@end

@interface ERBReusableViewController : UIViewController <AVAudioPlayerDelegate>

@property (retain, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (nonatomic, assign) id <ERBReusableViewControllerDelegate> delegate;
@property (retain, nonatomic) IBOutlet UIImageView *reusableImageView;
@property (nonatomic, assign) ERB_EXAM_STATE examState;
@property (nonatomic, retain) NSString *studentName;
@end
