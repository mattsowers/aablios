//
//  ERBBeginTutorialViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBLoginViewController.h"
#import "ERBScheduleViewController.h"
#import "ERBCheckInViewController.h"
#import "ERBTutorialViewController.h"
#import "ERBTutorialViewControllerOffline.h"
#import "ERBBeginSectionViewController.h"
#import "ERBExamViewController.h"
#import "ERBExamViewControllerOffline.h"
#import "ERBBreakViewController.h"
#import "ERBReusableViewController.h"
#import "ERBWebOperation.h"
#import "ERBQuestion.h"
#import "ERBCheckIn.h"
#import "DownloadViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ERBBeginTutorialViewController : UIViewController <ERBLoginViewControllerDelegate, ERBCheckInViewControllerDelegate, ERBScheduleViewController, ERBTutorialViewControllerDelegate, ERBBeginSectionViewControllerDelegate, ERBExamViewControllerDelegate, ERBBreakViewControllerDelegate, ERBReusableViewControllerDelegate, ERBWebServiceDelegate, AVAudioPlayerDelegate, DownloadViewControllerDelegate, UIAlertViewDelegate>{

    NSTimer *timer;
}

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *seatNumberLabel;
@property (retain, nonatomic) IBOutlet UILabel *putHeadphonesLabel;
@property (retain, nonatomic) IBOutlet UILabel *beginTutorialLabel;
@property (retain, nonatomic) IBOutlet UIButton *beginTutorialButton;
@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (assign, nonatomic) NSTimeInterval pauseStartTime;
@property (assign, nonatomic) NSTimeInterval pauseEndTime;
@property (assign, nonatomic) BOOL shouldHideStatusBar;
@property (nonatomic, assign) BOOL isExamResumed;

@property (assign, nonatomic) EXAM_PROGRESS_IDENTIFIER currentExamProgress;
@property (nonatomic, retain) ERBCheckIn *checkInData;
@property (nonatomic, retain) NSMutableArray *savedQuestionsArray;

- (IBAction)beginTutorialButtonPressed:(id)sender;
- (IBAction)backButtonPressed:(id)sender;
- (void) updateStatusBar;
- (void) uncheckStudent;

@end
