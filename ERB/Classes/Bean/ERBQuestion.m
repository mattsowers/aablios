//
//  ERBQuestion.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBQuestion.h"

@implementation ERBQuestion
@synthesize question;
@synthesize questionId;
@synthesize erbQuestionId;

- (void) dealloc
{
    [question release];
    [questionId release];
    [erbQuestionId release];
    [super dealloc];
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:question forKey:EXAM_QUESTION_KEY_QUESTION];
    [encoder encodeObject:questionId forKey:EXAM_QUESTION_KEY_QUESTION_ID];
    [encoder encodeObject:questionId forKey:EXAM_QUESTION_KEY_ERB_QUESTION_ID];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self.question = [decoder decodeObjectForKey:EXAM_QUESTION_KEY_QUESTION];
    self.questionId = [decoder decodeObjectForKey:EXAM_QUESTION_KEY_QUESTION_ID];
    self.questionId = [decoder decodeObjectForKey:EXAM_QUESTION_KEY_ERB_QUESTION_ID];
    return self;
}
@end
