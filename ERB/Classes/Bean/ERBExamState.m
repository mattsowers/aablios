//
//  ERBExamState.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBExamState.h"

@implementation ERBExamState

@synthesize currentSection, currentQuestionNumber, questionsArray, checkinData, currentExamState, currentExamProgress;

-(void)encodeWithCoder:(NSCoder *)encoder{

    [encoder encodeInt:currentSection forKey:EXAM_STATE_KEY_CURRENT_SECTION];
    [encoder encodeInt:currentQuestionNumber forKey:EXAM_STATE_KEY_CURRENT_QUESTION_NUMBER];
    [encoder encodeObject:questionsArray forKey:EXAM_STATE_KEY_QUESTIONS_ARRAY];
    [encoder encodeObject:checkinData forKey:EXAM_STATE_KEY_CHECKIN_DATA];
    [encoder encodeInt:currentExamState forKey:EXAM_STATE_KEY_CURRENT_STATE];
    [encoder encodeInt:currentExamProgress forKey:EXAM_STATE_KEY_CURRENT_PROGRESS];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self.currentSection = [decoder decodeIntForKey:EXAM_STATE_KEY_CURRENT_SECTION];
    self.currentQuestionNumber = [decoder decodeIntForKey:EXAM_STATE_KEY_CURRENT_QUESTION_NUMBER];
    self.questionsArray = [decoder decodeObjectForKey:EXAM_STATE_KEY_QUESTIONS_ARRAY];
    self.checkinData = [decoder decodeObjectForKey:EXAM_STATE_KEY_CHECKIN_DATA];
    self.currentExamState = [decoder decodeIntForKey:EXAM_STATE_KEY_CURRENT_STATE];
    self.currentExamProgress = [decoder decodeIntForKey:EXAM_STATE_KEY_CURRENT_PROGRESS];
    
    return self;
}


@end
