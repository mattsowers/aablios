//
//  ERBQuestion.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <Foundation/Foundation.h>

#define EXAM_QUESTION_KEY_QUESTION             @"question"
#define EXAM_QUESTION_KEY_QUESTION_ID          @"questionId"
#define EXAM_QUESTION_KEY_ERB_QUESTION_ID      @"erb_questionId"

@interface ERBQuestion : NSObject
@property (nonatomic, retain) NSString *question;
@property (nonatomic, retain) NSString *questionId;
@property (nonatomic, retain) NSString *erbQuestionId;
@end
