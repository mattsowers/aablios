//
//  ERBCheckIn.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBCheckIn.h"

@implementation ERBCheckIn
@synthesize examId, studentId, studentName, seatNumber;

- (void) dealloc
{
    [examId release];
    [studentId release];
    [studentName release];
    [seatNumber release];
    [super dealloc];
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:examId forKey:ERB_CHECKIN_KEY_EXAM_ID];
    [encoder encodeObject:studentId forKey:ERB_CHECKIN_KEY_STUDENT_ID];
    [encoder encodeObject:studentName forKey:ERB_CHECKIN_KEY_STUDENT_NAME];
    [encoder encodeObject:seatNumber forKey:ERB_CHECKIN_KEY_SEAT_NUMBER];
    
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self.examId = [decoder decodeObjectForKey:ERB_CHECKIN_KEY_EXAM_ID];
    self.studentId = [decoder decodeObjectForKey:ERB_CHECKIN_KEY_STUDENT_ID];
    self.studentName = [decoder decodeObjectForKey:ERB_CHECKIN_KEY_STUDENT_NAME];
    self.seatNumber = [decoder decodeObjectForKey:ERB_CHECKIN_KEY_SEAT_NUMBER];

    return self;
}

@end
