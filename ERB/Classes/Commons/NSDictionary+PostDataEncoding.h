//
//  NSDictionary+PostDataEncoding.h
//  ERB
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PostDataEncoding)

-(NSString*) urlEncodedKeyValueString;
-(NSString*) jsonEncodedKeyValueString;

@end
