//
//  ERBEventLogs.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "EventLogger.h"
#define ERB_EVENT_LOG_FILE_PATH        [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"EventLog.json"]

@interface EventLogger(private)

- (void)loadLogs;
- (void)callWebServiceForLogging:(id)json;

@end

@interface EventLogger ()

@property (nonatomic, retain) ERBWebOperation *webServiceHelper;

@end

@implementation EventLogger
@synthesize events, eventType, webServiceHelper;
@synthesize examId;
@synthesize studentId;

SYNTHESIZE_SINGLETON(EventLogger);


- (id)init {
    if (self = [super init]) {
        
        //Set the exam and student id to default values
        self.examId = [[NSUserDefaults standardUserDefaults] objectForKey:REQUEST_POST_KEY_EXAM_ID];
        self.studentId = [[NSUserDefaults standardUserDefaults] objectForKey:REQUEST_POST_KEY_STUDENT_ID];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:REQUEST_POST_KEY_EXAM_ID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:REQUEST_POST_KEY_STUDENT_ID];
        [USER_DEFAULTS synchronize];
        
        if (!self.examId) {
            self.examId = @"";
        }
        
        if (!self.studentId) {
            self.studentId = @"";
        }
        
        //Load the events logs into events array
        [self loadLogs];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
    SAFE_RELEASE(events);
    [examId release];
    [studentId release];
    [super dealloc];
}


-(void)saveLog:(NSDictionary *)logDict
{
    if (logDict && logDict.count > 0)
    {
        // Add the log dictionary into events array
        [self.events addObject:logDict];
        
        // Archive the events array by encoding it into a data object then atomically writing the resulting data object to a file at a given path.
        if (![NSKeyedArchiver archiveRootObject:self.events toFile:ERB_EVENT_LOG_FILE_PATH])
        {
            DLog(@"LOGGING NOT SUCCESSFUL");
        }
    }
}

#pragma mark - Private Methods

-(void)sendLogs
{
    // If there are no logs in the events array then return
    if (!self.events || [self.events count] == 0) {
        return;
    }
    
    //Crash fix: do not remove this code
    //Check to see if there is an empty dictionary in events array
    for (id dict in self.events) {
        if ([dict isKindOfClass:[NSDictionary class]] || [dict isKindOfClass:[NSMutableDictionary class]]) {
            if ([dict count] == 0) {
                [self.events removeObject:dict];
            }
        }
        else{
            [self.events removeObject:dict];
        }
        
    }
    
    NSError *error = nil;
    
    // Create a JSON object fronm the events array
    id logJSON = [NSJSONSerialization dataWithJSONObject:self.events options:NSJSONWritingPrettyPrinted error:&error];
    
    if (!error)
    {
        // Create a json string from the json data
        NSString *jsonString = [[[NSString alloc] initWithData:logJSON encoding:NSUTF8StringEncoding] autorelease];
        // Call Web service to log the json
        [self callWebServiceForLogging:jsonString];
    }
    
}

-(void)clearLogs{
    
    SAFE_RELEASE(events);
    events = [[NSMutableArray alloc] init];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:ERB_EVENT_LOG_FILE_PATH]) {
        [[NSData data] writeToFile:ERB_EVENT_LOG_FILE_PATH options:NSDataWritingAtomic error:nil];
    }
    
}

-(void)loadLogs{
    
    @try
    {
        //Create file if does not exist
        if (![[NSFileManager defaultManager] fileExistsAtPath:ERB_EVENT_LOG_FILE_PATH])
        {
            [[NSFileManager defaultManager] createFileAtPath:ERB_EVENT_LOG_FILE_PATH contents:nil attributes:nil];
        }
        
        // Decode the events array previously encoded by NSKeyedArchiver and then written to the file.
        self.events = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithFile:ERB_EVENT_LOG_FILE_PATH];
        
        // If events array is null, instantiate it
        if (!self.events) {
            events = [[NSMutableArray alloc] init];
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)callWebServiceForLogging:(id)json
{
    // Check for internet connection
    if ([[ERBCommons appController] internetCheck])
    {
        NSArray *keys = nil;
        NSArray *values = nil;
        
        NSString *userAgentString = [USER_DEFAULTS objectForKey:KEY_USER_AGENT];
        
        if (!userAgentString) {
            userAgentString = [ERBCommons getUserAgentString];
        }
        
        // create key and value rrays
        keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN,
                REQUEST_POST_KEY_EXAM_ID,
                REQUEST_POST_KEY_STUDENT_ID,
                REQUEST_POST_KEY_LOG_JSON,
                REQUEST_POST_KEY_LOG_TIMESTAMP,
                REQUEST_POST_KEY_LOG_TYPE,
                REQUEST_POST_KEY_USER_AGENT,
                REQUEST_POST_KEY_BUILD_VERSION, nil];
        
        values = [NSArray arrayWithObjects:[ERBCommons authToken],self.examId, self.studentId,self.events, [NSString stringWithFormat:@"%@",[NSDate date]],[ERBCommons getStringForLogType:self.eventType], userAgentString, [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"],nil];
        
        // Something has gone haywire, so check the values array
        if ([values count] != [keys count]) {
            DLog(@"Values for sending event logs: %@", values);
        }
        else{
            if (keys && values) {
                
                // Cancel the previous connection in case webservice is recalled, de-instantiate the webservice helper object
                if (webServiceHelper)
                {
                    CANCEL_CONNECTION(self.webServiceHelper);
                    [webServiceHelper release];
                    webServiceHelper = nil;
                }
                
                // Create the web service helper object to send the logs
                
                NSDictionary *eventLogParams = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
                
                webServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_SAVE_CLIENT_LOG_FILE]] withParams:eventLogParams withRequestIdentifier:REQUEST_ID_LOG_EVENTS withHttpMethod:ERB_REQUEST_POST];
                [eventLogParams release];
                self.webServiceHelper.delegate = self;
                [self.webServiceHelper sendWebServiceRequest];
                
            }
        }
    }
    else
    {
        [self performSelector:@selector(callWebServiceForLogging:) withObject:json afterDelay:5.0];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    //Check request Id
    if ([requestId isEqualToString:REQUEST_ID_LOG_EVENTS])
    {
        NSError *error = nil;
        NSDictionary *userActionResponse = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        if (!error) {
            if ([[userActionResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                // If logs are send successfully, then clear the logs
                [self clearLogs];
            }
        }
        else{
            DLog(@"Error while parsing JSON response for WS: %@", requestId);
        }
        
    }
}


- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if ([requestId isEqualToString:REQUEST_ID_LOG_EVENTS])
    {
        // Retry sending the logs after 10 seconds.
        [self performSelector:@selector(sendLogs) withObject:nil afterDelay:10.0];
    }
}

@end
