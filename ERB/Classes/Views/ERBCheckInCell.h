//
//  ERBCheckInCell.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>

@protocol ERBCheckInCellDelegate <NSObject>

- (void) didPressVerfiyWithTag: (NSInteger)tag;

@end

@interface ERBCheckInCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIButton *verifyButton;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (retain, nonatomic) IBOutlet UILabel *genderLabel;
@property (retain, nonatomic) IBOutlet UILabel *dobLabel;
@property (nonatomic, assign) id<ERBCheckInCellDelegate> delegate;

- (IBAction)verifyButtonPressed:(id)sender;
@end
