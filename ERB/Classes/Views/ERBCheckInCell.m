//
//  ERBCheckInCell.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBCheckInCell.h"

@implementation ERBCheckInCell
@synthesize delegate;
@synthesize verifyButton, nameLabel, genderLabel, dobLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [verifyButton release];
    [nameLabel release];
    [genderLabel release];
    [dobLabel release];
    [super dealloc];
}

- (IBAction)verifyButtonPressed:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(didPressVerfiyWithTag:)])
    {
        [delegate didPressVerfiyWithTag:verifyButton.tag];
    }
}
@end
