//
//  AudioVolumeViewController.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#define VOL_VIEW_CONTROLLER_TAG         -999999

@interface AudioVolumeViewController : UIViewController
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel;
@property (retain, nonatomic) IBOutlet UILabel *volumeNameLabel;
@property (retain, nonatomic) IBOutlet UIButton *plusButton;
@property (retain, nonatomic) IBOutlet UIButton *minusButton;

- (IBAction)minusVolumeButtonPressed:(id)sender;
- (IBAction)plusVolumeButtonPressed:(id)sender;

@end
