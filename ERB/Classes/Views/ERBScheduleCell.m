//
//  ERBScheduleCell.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */
#import "ERBScheduleCell.h"

@implementation ERBScheduleCell
@synthesize delegate;
@synthesize timeLabel, examLabel, supervisorLabel, studentsLabel, administratorsLabel, launchButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [timeLabel release];
    [examLabel release];
    [supervisorLabel release];
    [studentsLabel release];
    [administratorsLabel release];
    [launchButton release];
    [super dealloc];
}

#pragma Actions
- (IBAction)launchExamButtonPressed:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(didPressLaunchNowWithTag:)])
    {
        [delegate didPressLaunchNowWithTag:launchButton.tag];
    }
}

@end
