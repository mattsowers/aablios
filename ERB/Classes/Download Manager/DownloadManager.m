//
//  DownloadManager.m
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import "DownloadManager.h"
#import "Download.h"

@interface DownloadManager () <DownloadDelegate>

@end

@implementation DownloadManager

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _downloads = [[NSMutableArray alloc] init];
        _maxConcurrentDownloads = 12;
    }
    
    return self;
}

-(void)dealloc{
    SAFE_RELEASE(_downloads);
    [super dealloc];
}

#pragma mark - DownloadManager public methods

- (void)addDownloadWithFilePath:(NSString *)filePath URL:(NSURL *)url md5Hash:(NSString *)hash
{
    Download *download = [[Download alloc] init];
    download.filePath = filePath;
    download.url = url;
    download.md5Hash = hash;
    download.delegate = self;
    [self.downloads addObject:download];
    [download release];
}

- (void)start
{
    [self tryDownloading];
}

- (void)cancelAll
{
    for (Download *download in self.downloads) {
        download.reportDownloadFailure = NO;
        [download cancel];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCancelAllForManager:)])
    {
        [self.delegate didCancelAllForManager:self];
    }
    
}

#pragma mark - DownloadDelegate Methods

- (void)downloadDidFinishLoading:(Download *)download
{
    [self.downloads removeObject:download];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(downloadManager:downloadDidFinishLoading:)])
    {
        [self.delegate downloadManager:self downloadDidFinishLoading:download];
    }
    
    [self tryDownloading];
}

- (void)downloadDidFail:(Download *)download
{
    
    [self.downloads removeObject:download];
    if (self.delegate && [self.delegate respondsToSelector:@selector(downloadManager:downloadDidFail:)])
        [self.delegate downloadManager:self downloadDidFail:download];
    [self tryDownloading];
    
}

- (void)downloadDidReceiveData:(Download *)download
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(downloadManager:downloadDidReceiveData:)])
    {
        [self.delegate downloadManager:self downloadDidReceiveData:download];
    }
}

#pragma mark - Private methods

- (void)informDelegateThatDownloadsAreDone
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didFinishLoadingAllForManager:)])
    {
        [self.delegate didFinishLoadingAllForManager:self];
    }
}

- (void)tryDownloading
{
    NSInteger totalDownloads = [self.downloads count];
    
    // if we're done, inform the delegate
    
    if (totalDownloads == 0)
    {
        [self informDelegateThatDownloadsAreDone];
        return;
    }
    
    // while there are downloads waiting to be started and we haven't hit the maxConcurrentDownloads, then start
    
    while ([self countUnstartedDownloads] > 0 && [self countActiveDownloads] < self.maxConcurrentDownloads)
    {
        for (Download *download in self.downloads)
        {
            if (!download.isDownloading)
            {
                [download start];
                break;
            }
        }
    }
}

- (NSInteger)countUnstartedDownloads
{
    return [self.downloads count] - [self countActiveDownloads];
}

- (NSInteger)countActiveDownloads
{
    NSInteger activeDownloadCount = 0;
    
    for (Download *download in self.downloads)
    {
        if (download.isDownloading)
            activeDownloadCount++;
    }
    
    return activeDownloadCount;
}

@end
