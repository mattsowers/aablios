//
//  DownloadViewController.h


#import <UIKit/UIKit.h>
#import "SSZipArchive.h"

@protocol DownloadViewControllerDelegate <NSObject>
- (void) downloadsDidFinishSucessfully;
- (void) failedToDownloadAssets;
@end

@interface DownloadViewController : UIViewController<SSZipArchiveDelegate, UIAlertViewDelegate>
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;
@property (retain, nonatomic) IBOutlet UIButton *downloadButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic, assign) id <DownloadViewControllerDelegate> delegate;
@property (nonatomic, retain) NSDictionary *downloadDict;
@property (nonatomic, retain) NSArray *deletedItems;
@property (nonatomic, retain) NSArray *updatedItems;
@property (nonatomic, retain) NSString *currentRevision;
@property (retain, nonatomic) IBOutlet UILabel *progressLabel;
@property (nonatomic, retain) NSArray *downloadsArray;
@property (retain, nonatomic) IBOutlet UILabel *lastSyncLabel;

- (IBAction)downloadButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
@end
