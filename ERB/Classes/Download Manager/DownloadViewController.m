//
//  DownloadViewController.m



#import "DownloadViewController.h"
#import "DownloadManager.h"
#import "FileHash.h"

// Order of tasks:
// Check for downloads/updates
// Once download/update finishes or there are no downloads/updates,
// check for assets to be deleted and if any then delete them

@interface DownloadViewController () <DownloadManagerDelegate>

@property (retain, nonatomic) DownloadManager *downloadManager;
@property (nonatomic, retain) NSMutableArray *unsuccessfulDownloads;
@property (nonatomic, retain) NSMutableArray *successfulDownloads;
@property (nonatomic, assign) long long totalDownloadSize;
@property (nonatomic, assign) long long actualDownloadProgress;
@property (nonatomic, assign) NSTimeInterval downloadStartInterval;
@property (nonatomic, assign) NSTimeInterval appEnteredBackgroundTime;
@property (nonatomic, assign) NSTimeInterval appEnteredForegroundTime;

- (void) queueAndStartDownloads;
- (void) configureUI;
- (void) retryDownloading;
- (void) deleteAssets;
- (void) updateAssets;
- (NSString *) getUnZipPath;
- (void) startUnCompressing;
- (void) moveFilesForBackgroundDirectory:(NSString *)backgroundDirectory;
- (void) onDownloadCompletion;
- (void) appEnteredBackground;
- (void) appEnteredForeground;

@end

@implementation DownloadViewController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!_unsuccessfulDownloads) {
        _unsuccessfulDownloads = [[NSMutableArray alloc] init];
    }
    
    if (!_successfulDownloads) {
        _successfulDownloads = [[NSMutableArray alloc] init];
    }
    
    self.view.hidden = YES;
    
    self.totalDownloadSize = 0.0;
    self.actualDownloadProgress = 0.0;
    self.appEnteredBackgroundTime = 0.0;
    self.appEnteredForegroundTime = 0.0;
    
    if ([USER_DEFAULTS objectForKey:KEY_LAST_SYNC_DATE]) {
        self.lastSyncLabel.hidden = NO;
        self.lastSyncLabel.text = [NSString stringWithFormat:@"Last synced on %@", [ERBCommons getStringFromDate:[USER_DEFAULTS objectForKey:KEY_LAST_SYNC_DATE] inFormat:REQUIRED_DATE_FORMAT]];
    }
    
    // Clean up the documents directory
    if ([[ERBCommons getCurrentAssetsRevision] isEqualToString:@"0"]) {
        // Indicates incomplete assets download
        if ([[NSFileManager defaultManager] fileExistsAtPath:RESOURCES_DIRECTORY_PATH]) {
            [[NSFileManager defaultManager] removeItemAtPath:RESOURCES_DIRECTORY_PATH error:nil];
        }
    }
    // Indicates incomplete update assets download
    if ([[NSFileManager defaultManager] fileExistsAtPath:UPDATES_DIRECTORY_PATH]) {
        [[NSFileManager defaultManager] removeItemAtPath:UPDATES_DIRECTORY_PATH error:nil];
    }
    
    for (NSDictionary *assetDict in self.downloadsArray)
    {
        self.totalDownloadSize += [[assetDict objectForKey:RESPONSE_KEY_DOWNLOAD_SIZE] floatValue];
    }
    
    self.messageLabel.text = [NSString stringWithFormat:@"Please download %0.2f GB of assets..",(double)(self.totalDownloadSize/(1024.0*1024.0*1024.0))];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    // Configure the UI
    [self performSelector:@selector(configureUI) withObject:nil afterDelay:2];
    
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setDownloadButton:nil];
    [self setCancelButton:nil];
    [self setMessageLabel:nil];
    [self setProgressLabel:nil];
    [super viewDidUnload];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SAFE_RELEASE(_downloadManager);
    SAFE_RELEASE(_cancelButton);
    SAFE_RELEASE(_downloadButton);
    SAFE_RELEASE(_unsuccessfulDownloads);
    SAFE_RELEASE(_successfulDownloads);
    SAFE_RELEASE(_deletedItems);
    SAFE_RELEASE(_currentRevision);
    SAFE_RELEASE(_downloadsArray);
    SAFE_RELEASE(_updatedItems);
    SAFE_RELEASE(_messageLabel);
    SAFE_RELEASE(_progressLabel)
    SAFE_RELEASE(_activityIndicator);
    [_lastSyncLabel release];
    [super dealloc];
}

#pragma mark - IBAction methods

- (IBAction)downloadButtonPressed:(id)sender {
    
    if ([[ERBCommons appController] internetCheck]){
        [self queueAndStartDownloads];
        self.downloadButton.enabled = NO;
        self.cancelButton.enabled = YES;
        self.progressLabel.hidden = NO;
        self.messageLabel.text = @"Please wait while assets are downloaded..";
        self.downloadStartInterval = [NSDate timeIntervalSinceReferenceDate];
        [self.activityIndicator startAnimating];
    }
    else{
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
    
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.downloadManager cancelAll];
}

#pragma mark - Private Methods

- (void) configureUI{
    
    if (self.view.isHidden) {
        self.view.hidden = NO;
    }
    // Check that there are no updates to download
    if (self.downloadsArray.count == 0/*(NSArray *)[NSNull null]*/) {
        // Check if there are files to be deleted
        if (self.deletedItems.count != 0 /*(NSArray *)[NSNull null]*/) {
            // Modify UI
            self.messageLabel.text = @"Please wait while assets are updated..";
            self.downloadButton.enabled = NO;
            self.cancelButton.enabled = NO;
            self.progressLabel.hidden = YES;
            // Call delete assets method
            [self deleteAssets];
        }
        else{
            [ERBCommons setCurrentAssetsRevision:self.currentRevision];
            if (self.delegate && [self.delegate respondsToSelector:@selector(downloadsDidFinishSucessfully)]) {
                [self.delegate downloadsDidFinishSucessfully];
            }
            SAVE_USER_DEFAULTS_AND_SYNC([NSDate date], KEY_LAST_SYNC_DATE);
        }
    }
}

- (void) queueAndStartDownloads{
    
    // create download manager instance
    SAFE_RELEASE(_downloadManager);
    _downloadManager = [[DownloadManager alloc] init];
    self.downloadManager.delegate = self;
    self.downloadManager.maxConcurrentDownloads = 1;
    
    // queue the files to be downloaded
    
    for (NSDictionary *assetDict in self.downloadsArray)
    {
        NSString *downloadFolder = RESOURCES_DIRECTORY_PATH;
        NSString *urlString = [assetDict objectForKey:RESPONSE_KEY_DOWNLOAD_PATH];
        NSString *zipHash = [assetDict objectForKey:RESPONSE_KEY_DOWNLOAD_ZIP_HASH];
        NSString *fileName = urlString;
        fileName = [fileName stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        fileName = [[fileName componentsSeparatedByString:@"/"] lastObject];
        
        NSString *downloadFilePath = [downloadFolder stringByAppendingPathComponent:[fileName lastPathComponent]];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        [self.downloadManager addDownloadWithFilePath:downloadFilePath URL:url md5Hash:zipHash];
    }
    
    [self.downloadManager start];
    
}

- (void) updateAssets{
    
    for (NSString *filePath in self.updatedItems) {
        
        NSError *error = nil;
        
        // Get the path of file in Update directory
        NSString *sourcePath = [UPDATES_DIRECTORY_PATH stringByAppendingPathComponent:filePath];
        NSString *destinationPath = @"";
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:sourcePath]) {
            
            // Get the path of the file that needs to be updated
            if ([sourcePath rangeOfString:ASSETS_BACKGROUND_DIRECTORY_PREFIX].location != NSNotFound) {
                destinationPath = [EXAM_ASSETS_BACKGROUND_PATH stringByAppendingPathComponent:[filePath lastPathComponent]];
            }
            else{
                destinationPath = [RESOURCES_DIRECTORY_PATH stringByAppendingPathComponent:filePath];
            }
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:[destinationPath stringByDeletingLastPathComponent]]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:[destinationPath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];
            }
            
            // If file exists at the update path, then delete it
            if ([[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
                [[NSFileManager defaultManager] removeItemAtPath:destinationPath error:nil];
            }
            
            // Move the file from "Updates" directory to the desired path
            BOOL success = [[NSFileManager defaultManager] moveItemAtPath:sourcePath toPath:destinationPath error:&error];
            
            if (!success) DLog(@"Error: %@ while moving file to path: %@", error.localizedDescription, destinationPath);
        }
        
    }
    
    // Once all updates are done delete the Update directory
    [[NSFileManager defaultManager] removeItemAtPath:UPDATES_DIRECTORY_PATH error:nil];
    // Set the download array as null
    self.downloadsArray = [NSArray array];//(NSArray *)[NSNull null];
    
    // Call the configure UI method to check if there are any assets that need to be deleted or not
    [self configureUI];
    
}

- (void) deleteAssets{
    
    for (NSString *deletePath in self.deletedItems) {
        
        NSError *error = nil;
        
        // Create actual delete path for file from the relative path provided in array and remove the item from that path
        deletePath = [RESOURCES_DIRECTORY_PATH stringByAppendingPathComponent:deletePath];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:deletePath]) {
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:deletePath error:&error];
            if (!success) DLog(@"Error: %@ while deleting file at path: %@", error.localizedDescription, deletePath);
        }
        
    }
    
    // After assets have been deleted update the revison number and call downloadsDidFinishSucessfully delegate
    self.deletedItems = [NSArray array];//(NSArray *)[NSNull null];
    [self configureUI];
    
}

- (void) retryDownloading{
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    
    for(Download *download in self.unsuccessfulDownloads){
        NSDictionary *dict = @{RESPONSE_KEY_DOWNLOAD_ZIP_HASH : download.md5Hash,
                               RESPONSE_KEY_DOWNLOAD_PATH : [[download url] absoluteString],
                               RESPONSE_KEY_DOWNLOAD_SIZE : [NSString stringWithFormat:@"%lld", download.expectedContentLength]};
        [temp addObject:dict];
    }
    
    self.downloadsArray = temp;
    SAFE_RELEASE(temp);
    [self.unsuccessfulDownloads removeAllObjects];
    [self queueAndStartDownloads];
}

- (NSString *) getUnZipPath{
    
    NSString *unZipPath = nil;
    
    if (self.updatedItems.count != 0) {
        unZipPath = UPDATES_DIRECTORY_PATH;
    }
    else{
        unZipPath = RESOURCES_DIRECTORY_PATH ;
    }
    
    // Create unzip directory, if not already present
    if (![[NSFileManager defaultManager] fileExistsAtPath:unZipPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:unZipPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return unZipPath;
}

- (void) startUnCompressing{
    
    // This will be done zip by zip
    if (self.successfulDownloads.count != 0) {
        Download *download = [self.successfulDownloads objectAtIndex:0];
        if (download) {
            NSString *zipPath = download.filePath;
            NSString *unZipPath = [self getUnZipPath];
            if ([SSZipArchive unzipFileAtPath:zipPath toDestination:unZipPath delegate:self]) {
                NSLog(@"SUCCESS UNZIP");
            }
            else{
                NSLog(@"FAILED UNZIP");
            }
        }
    }
    else{
        [self onDownloadCompletion];
    }
}

- (void) moveFilesForBackgroundDirectory:(NSString *)backgroundDirectory{
    
    backgroundDirectory = [[backgroundDirectory stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"background"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:backgroundDirectory]) {
        NSArray *filelist = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:backgroundDirectory error: nil];
        for (NSString *file in filelist) {
            
            NSError *error = nil;
            NSString *sourcePath = [backgroundDirectory stringByAppendingPathComponent:file];
            NSString *destinationPath = [EXAM_ASSETS_BACKGROUND_PATH stringByAppendingPathComponent:file];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:EXAM_ASSETS_BACKGROUND_PATH]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:EXAM_ASSETS_BACKGROUND_PATH withIntermediateDirectories:YES attributes:nil error:nil];
            }
            
            // If file exists at the destination path, then delete it
            if ([[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
                [[NSFileManager defaultManager] removeItemAtPath:destinationPath error:nil];
            }
            
            // Move the file from source directory to the desired path
            BOOL success = [[NSFileManager defaultManager] moveItemAtPath:sourcePath toPath:destinationPath error:&error];
            
            if (!success) DLog(@"Error: %@ while moving file at path: %@", error.localizedDescription, sourcePath);
        }
        
        // Remove the empty directory
        [[NSFileManager defaultManager] removeItemAtPath:backgroundDirectory error:nil];
    }
}

- (void) onDownloadCompletion{
    // Check if there are assets to be updated, if yes then modify UI and update the assets
    if (self.updatedItems.count != 0) {
        self.messageLabel.text = @"Please wait while assets are updated..";
        self.downloadButton.enabled = NO;
        self.cancelButton.enabled = NO;
        self.progressLabel.hidden = YES;
        
        // To update the UI
        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        
        [self updateAssets];
    }
    else{
        [ERBCommons setCurrentAssetsRevision:self.currentRevision];
        if (self.delegate && [self.delegate respondsToSelector:@selector(downloadsDidFinishSucessfully)]) {
            [self.delegate downloadsDidFinishSucessfully];
            SAVE_USER_DEFAULTS_AND_SYNC([NSDate date], KEY_LAST_SYNC_DATE);
        }
    }
}

- (void) appEnteredBackground{
    self.appEnteredBackgroundTime = [NSDate timeIntervalSinceReferenceDate];
}

- (void) appEnteredForeground{
    self.appEnteredForegroundTime = [NSDate timeIntervalSinceReferenceDate];
}

#pragma mark - DownloadManager Delegate Methods

- (void)downloadManager:(DownloadManager *)downloadManager downloadDidReceiveData:(Download *)download{
    
    self.actualDownloadProgress += download.dataLength;
    
    long long remaining = self.totalDownloadSize - self.actualDownloadProgress;
    NSTimeInterval timeElapsed = [NSDate timeIntervalSinceReferenceDate] - self.downloadStartInterval - (self.appEnteredForegroundTime - self.appEnteredBackgroundTime);
    double timeRemaining = (double)((timeElapsed/self.actualDownloadProgress)*remaining);
    
    double progressPercent = ((double)self.actualDownloadProgress/self.totalDownloadSize)*100;
    
    self.progressLabel.text = [NSString stringWithFormat:@"Downloaded : %0.2f %% \nTime left : %@", progressPercent, [ERBCommons getTimeElapsedForInterval:timeRemaining]];
}

- (void)downloadManager:(DownloadManager *)downloadManager downloadDidFinishLoading:(Download *)download{
    
    if (![download.md5Hash isEqualToString:[FileHash md5HashOfFileAtPath:download.filePath]]) {
        DLog(@"%s UnFinished Downloading of File: %@", __FUNCTION__, download.filePath);
        [self.unsuccessfulDownloads addObject:download];
    }
    else{
        DLog(@"%s Finished Downloading of File: %@", __FUNCTION__, download.filePath);
        [self.successfulDownloads addObject:download];
    }
    
}

- (void)didFinishLoadingAllForManager:(DownloadManager *)downloadManager{
    
    // To update the UI
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    
    if (self.unsuccessfulDownloads.count == 0){
        
        self.messageLabel.text = @"Please wait while assets are uncompressed..";
        self.downloadButton.enabled = NO;
        self.cancelButton.enabled = NO;
        self.progressLabel.hidden = YES;
        
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"filePath"  ascending:YES];
        [self.successfulDownloads sortUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        SAFE_RELEASE(descriptor);
        
        // To update the UI
        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        
        // Once all downloads are done, start uncompressing the zips
        [self startUnCompressing];
    }
    else{
        //        self.progressLabel.hidden = YES;
        NSString *message = [NSString stringWithFormat:@"Some error occured while downloading the assets. Do you want to retry?"];
        [ERBCommons showAlert:message delegate:self btnTitle:@"Cancel" otherBtnTitle:@"Retry" tag:1];
    }
}

- (void)downloadManager:(DownloadManager *)downloadManager downloadDidFail:(Download *)download{
    DLog(@"%s %@ error=%@", __FUNCTION__, download.filePath, download.error);
    [self.unsuccessfulDownloads addObject:download];
    self.actualDownloadProgress -= download.expectedContentLength;
}

- (void) didCancelAllForManager:(DownloadManager *)downloadManager{
    
    // Set up the UI
    self.downloadButton.enabled = YES;
    self.cancelButton.enabled = NO;
    self.progressLabel.hidden = YES;
    self.messageLabel.text = @"Assets not downloaded. Please download them to continue.";
    
    // Clean up Documents Directory
    
    if (self.updatedItems.count != 0) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:UPDATES_DIRECTORY_PATH]) {
            [[NSFileManager defaultManager] removeItemAtPath:UPDATES_DIRECTORY_PATH error:nil];
        }
    }
    else{
        if ([[NSFileManager defaultManager] fileExistsAtPath:RESOURCES_DIRECTORY_PATH]) {
            [[NSFileManager defaultManager] removeItemAtPath:RESOURCES_DIRECTORY_PATH error:nil];
        }
    }
    
    // Clean up the arrays
    [self.unsuccessfulDownloads removeAllObjects];
    [self.successfulDownloads removeAllObjects];
    
    SAFE_RELEASE(_downloadManager);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(failedToDownloadAssets)]) {
        [self.delegate failedToDownloadAssets];
    }
    
}

#pragma mark - SSZipArchiveDelegate Methods

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath{
    
    DLog(@"Successfully unzipped file Archive at path: %@ to Path: %@", path, unzippedPath);
    // Delete the zip
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    
    // Move the backround files to alpha
    if ([path rangeOfString:ASSETS_BACKGROUND_DIRECTORY_PREFIX].location != NSNotFound) {
        [self moveFilesForBackgroundDirectory:[unzippedPath stringByAppendingPathComponent:[[path lastPathComponent] stringByDeletingPathExtension]]];
    }
    
    // If all zips have been uncompressed, call download completion method
    // else remove the uncompressed zip from successful downloads array and start uncompressing process for next zip in array.
    if (self.successfulDownloads.count == 0) {
        [self onDownloadCompletion];
    }
    else{
        [self.successfulDownloads removeObjectAtIndex:0];
        [self startUnCompressing];
    }
}

#pragma mark - UIAlertViewDelegate Methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            [self retryDownloading];
        }
        else if (buttonIndex == 0){
            [self didCancelAllForManager:nil];
        }
    }
}

@end
